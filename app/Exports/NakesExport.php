<?php

namespace App\Exports;

use App\Nakes;
use Maatwebsite\Excel\Concerns\FromCollection;

class NakesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Nakes::all();
    }
}
