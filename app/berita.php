<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class berita extends Model
{
    protected $table = 'berita';
    protected $fillable = ["judul", "penulis", "konten", "foto"];
    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }
}
