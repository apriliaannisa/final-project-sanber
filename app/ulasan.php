<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ulasan extends Model
{
    protected $table = 'ulasan';
    protected $fillable = [
        'user_id',
        'berita_id',
        'konten'
    ];
    public function berita()
    {
        return $this->belongsToMany('App\Berita');
    }

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
