<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = 'profile';
    protected $fillable = [
        'user_id',
        'umur',
        'alamat',
        'foto'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
