<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kartu_ucapan extends Model
{
    protected $table = 'kartu_ucapan';
    protected $fillable = [
        'user_id',
        'nakes_id',
        'judul',
        'konten',
        'foto'
    ];
    public function user()
    {
        return $this->belongsToMany('App\User');
    }

    public function nakes()
    {
        return $this->belongsToMany('App\Nakes');
    }
}
