<?php

namespace App\Http\Controllers\Corotana;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\kartu_ucapan;
use App\nakes;
use App\User;

class KartuUcapanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kartu_ucapan = kartu_ucapan::all();
        return view('admin.pages_curd.kartu.index', compact('kartu_ucapan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nakes = nakes::all();
        $user = user::all();
        return view('admin.pages_curd.kartu.create', compact('nakes', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'nakes_id' => 'required',
            'judul' => 'required',
            'konten' => 'required',
        ]);
        $nakes = kartu_ucapan::create([
            'user_id' => $request['user_id'],
            'nakes_id' => $request['nakes_id'],
            'judul' => $request['judul'],
            'konten' => $request['konten'],
            'foto' => $request['foto']
        ]);
        return redirect('/nakes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kartu_ucapan = kartu_ucapan::find($id);
        return view('admin.pages_curd.kartu.show', compact('kartu_ucapan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kartu_ucapan = kartu_ucapan::find($id);
        return view('admin.pages_curd.kartu.show', compact('kartu_ucapan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',
            'nakes_id' => 'required',
            'judul' => 'required',
            'konten' => 'required',
        ]);
        $kartu_ucapan = kartu_ucapan::find($id);
        $kartu_ucapan->user_id = $request->user_id;
        $kartu_ucapan->nakes_id = $request->nakes_id;
        $kartu_ucapan->judul = $request->judul;
        $kartu_ucapan->konten = $request->konten;
        $kartu_ucapan->foto = $request->foto;
        $kartu_ucapan->update();
        return redirect('/kartu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kartu_ucapan = kartu_ucapan::find($id);
        $kartu_ucapan->delete();
        return redirect('/kartu_ucapan');
    }
}
