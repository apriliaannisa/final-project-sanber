<?php

namespace App\Http\Controllers\Corotana;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\nakes;

class NakesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nakes = nakes::all();
        return view('admin.pages_curd.nakes.index', compact('nakes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages_curd.nakes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'posisi' => 'required',
            'bio' => 'required'
        ]);
        $nakes = nakes::create([
            'nama' => $request['nama'],
            'posisi' => $request['posisi'],
            'bio' => $request['bio'],
            'foto' => $request['foto']
        ]);
        return redirect('/nakes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nakes = nakes::find($id);
        return view('admin.pages_curd.nakes.show', compact('nakes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nakes = nakes::find($id);
        return view('admin.pages_curd.nakes.show', compact('nakes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'posisi' => 'required',
            'bio' => 'required'
        ]);
        $nakes = nakes::find($id);
        $nakes->nama = $request->nama;
        $nakes->posisi = $request->posisi;
        $nakes->bio = $request->bio;
        $nakes->foto = $request->foto;
        $nakes->update();
        return redirect('/nakes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nakes = nakes::find($id);
        $nakes->delete();
        return redirect('/nakes');
    }
}
