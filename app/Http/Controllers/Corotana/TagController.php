<?php

namespace App\Http\Controllers\Corotana;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\tag;

class TagController extends Controller
{
    protected function index()
    {
        $tag = tag::all();
        return view('admin.pages_curd.tag.index', compact('tag'));
    }

    protected function create()
    {
        return view('admin.pages_curd.tag.create');
    }

    protected function store(Request $request)
    {
        $request->validate([
            'berita_id' => 'required',
            'nama' => 'required'
        ]);
        tag::create([
            'berita_id' => $request['berita_id'],
            'nama'  => $request['nama']
        ]);
        return redirect('/tag');
    }

    protected function show($id)
    {
        $tag = tag::find($id);
        return view('admin.pages_curd.tag.show', compact('tag'));
    }

    protected function edit($id)
    {
        $tag = tag::find($id);
        return view('admin.pages_curd.tag.edit', compact('tag'));
    }

    protected function update($id, Request $request)
    {
        $request->validate([
            'berita_id' => 'required',
            'nama' => 'required'
        ]);
        $tag = tag::find($id);
        $tag->berita_id = $request->berita_id;
        $tag->nama = $request->nama;
        $tag->update();
        return redirect('/tag');
    }

    protected function destroy($id)
    {
        $tag = tag::find($id);
        $tag->delete();
        return redirect('/tag');
    }
}
