<?php

namespace App\Http\Controllers\Corotana;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\berita;

class BeritaController extends Controller
{
    protected function index()
    {
        $berita = berita::all();
        return view('admin.pages_curd.berita.index', compact('berita'));
    }

    protected function create()
    {
        return view('admin.pages_curd.berita.create');
    }

    protected function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'konten'  => 'required',
            'penulis' => 'required'
        ]);
        berita::create([
            "judul" => $request["judul"],
            "konten"  => $request["konten"],
            "penulis" => $request["penulis"],
            'foto' => $request['foto']
        ]);
        return redirect('/berita');
    }

    protected function show($id)
    {
        $berita = berita::find($id);
        return view('admin.pages_curd.berita.show', compact('berita'));
    }

    protected function edit($id)
    {
        $berita = berita::find($id);
        return view('admin.pages_curd.berita.edit', compact('berita'));
    }

    protected function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'konten' => 'required',
            'penulis' => 'required'
        ]);
        $berita = berita::find($id);
        $berita->judul = $request->judul;
        $berita->konten = $request->konten;
        $berita->penulis = $request->penulis;
        $berita->foto = $request->foto;
        $berita->update();
        return redirect('/berita');
    }

    protected function destroy($id)
    {
        $berita = berita::find($id);
        $berita->delete();
        return redirect('/berita');
    }
}
