<?php

namespace App\Http\Controllers\Corotana;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ulasan;

class UlasanController extends Controller
{
    protected function index()
    {
        $ulasan = ulasan::all();
        return view('admin.pages_curd.ulasan.index', compact('ulasan'));
    }

    protected function create()
    {
        return view('admin.pages_curd.ulasan.create');
    }

    protected function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'berita_id'  => 'required',
            'konten' => 'required'
        ]);
        ulasan::create([
            'user_id' => $request['user_id'],
            'berita_id'  => $request['berita_id'],
            'konten' => $request['konten']
        ]);
        return redirect('/ulasan');
    }

    protected function show($id)
    {
        $ulasan = ulasan::find($id);
        return view('admin.pages_curd.ulasan.show', compact('ulasan'));
    }

    protected function edit($id)
    {
        $ulasan = ulasan::find($id);
        return view('admin.pages_curd.ulasan.edit', compact('ulasan'));
    }

    protected function update($id, Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'berita_id'  => 'required',
            'konten' => 'required'
        ]);
        $ulasan = ulasan::find($id);
        $ulasan->user_id = $request->user_id;
        $ulasan->berita_id = $request->berita_id;
        $ulasan->konten = $request->konten;
        $ulasan->update();
        return redirect('/ulasan');
    }

    protected function destroy($id)
    {
        $ulasan = ulasan::find($id);
        $ulasan->delete();
        return redirect('/ulasan');
    }
}
