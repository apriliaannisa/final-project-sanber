<?php

namespace App\Http\Controllers\Corotana;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\profile;

class ProfileController extends Controller
{
    protected function index()
    {
        $profile = profile::all();
        return view('admin.pages_curd.profile.index', compact('profile'));
    }

    protected function create()
    {
        return view('admin.pages_curd.profile.create');
    }

    protected function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'umur' => 'required',
            'alamat' => 'required'
        ]);
        profile::create([
            'user_id' => $request['user_id'],
            'umur' => $request['umur'],
            'alamat' => $request['alamat'],
            'foto' => $request['foto']
        ]);
        return redirect('/profile');
    }

    protected function show($id)
    {
        $profile = profile::find($id);
        return view('admin.pages_curd.profile.show', compact('profile'));
    }

    protected function edit($id)
    {
        $profile = profile::find($id);
        return view('admin.pages_curd.profile.edit', compact('profile'));
    }

    protected function update($id, Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'umur' => 'required',
            'alamat' => 'required'
        ]);
        $profile = profile::find($id);
        $profile->user_id = $request->user_id;
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->foto = $request->foto;
        $profile->update();
        return redirect('/profile');
    }

    protected function destroy($id)
    {
        $profile = profile::find($id);
        $profile->delete();
        return redirect('/profile');
    }
}
