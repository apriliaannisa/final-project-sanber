<?php

namespace App\Http\Controllers\Corotana;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\NakesExport;
use Maatwebsite\Excel\Facades\Excel;
use App\admin;
use App\user;
use App\nakes;
use PDF;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function home()
    {
        return view('admin.pages_master.opsi');
    }
    public function getUser()
    {
        $data = "";
        $users = User::with('profile')->get();
        foreach ($users as $user) {
            $data = $data .
                '<tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->Nama }}</td> 
            <td>{{ $user->Email }}</td>
            <td>{{ $user->Umur }}</td>
            <td>{{ $user->Alamat }}</td>
            </tr>';
        }
        $pdf = PDF::loadView('admin.pages_master.getuser', compact('data'));
        return $pdf->stream();
    }

    public function export()
    {
        return Excel::download(new NakesExport, 'nakes.xlsx');
    }
}
