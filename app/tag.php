<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tag extends Model
{
    protected $table = 'tag';
    protected $fillable = [
        'berita_id',
        'nama'
    ];
    public function berita()
    {
        return $this->hasMany('App\Berita');
    }
}
