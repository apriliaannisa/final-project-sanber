<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nakes extends Model
{
    protected $table = 'nakes';
    protected $fillable = [
        'nama',
        'posisi',
        'bio',
        'foto'
    ];
}
