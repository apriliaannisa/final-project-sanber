<!DOCTYPE html>
<html lang="en">
<head>
     <!-- basic -->
     <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Covido</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="{{asset('covido-master/css/bootstrap.min.css')}}">
      
      <!-- style css -->
      <link rel="stylesheet" href="{{asset('covido-master/css/style.css')}}">
      <!-- Responsive-->
      <link rel="stylesheet" href="{{asset('covido-master/css/responsive.css')}}">
      <!-- fevicon -->     
       {{-- <link rel="icon" href="{{asset('covido-master/images/fevicon.png')}}" type="image/gif" /> --}}
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
       <link rel="stylesheet" href="{{asset('covido-master/css/owl.carousel.min.css')}}"> 
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body class="main-layout inner_page" style="background-image: url('covido-master/images/bg-protection.png')">
   <!-- loader  -->
   <div class="loader_bg">
      <div class="loader"><img src="{{asset('covido-master/images/loading.gif')}}" alt="#"/></div>
   </div>
   <!-- end loader -->
   <!-- top -->
   <!-- header -->
      <header class="header-area">
         <div class="left">
            <a href="Javascript:void(0)"><i class="fa fa-search" aria-hidden="true"></i></a>
         </div>
         <div class="right">
            <a href="Javascript:void(0)"><i class="fa fa-user" aria-hidden="true"></i></a>
         </div>
         <div class="container">
            <div class="row d_flex">
               <div class="col-sm-3 logo_sm">
                  <div class="logo">
                     <a href="/home"></a>
                  </div>
               </div>
               <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-9">
                  <div class="navbar-area">
                     <nav class="site-navbar">
                        <ul>
                           <li><a href="/">Home</a></li>
                           <li><a href="/about">About</a></li>
                           <li><a href="/action">Card</a></li>
                           <li><a href="/home">Covido</a></li>
                           <li><a href="/news">News</a></li>
                           <li><a href="/workers">Workers</a></li>
                           <li><a href="/contact">Contact </a></li>
                        </ul>
                        <button class="nav-toggler">
                          <span></span>
                        </button>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
      </header>

      <!--protect-->
<section>
      <h1 style ="text-align:center; font-weight:bold; font-family: Serif; font-size:50px">Prevent</h1>
      <div id="article-list" style= "margin:30px; color:black">
        <article>
          <a href="#" style="font-weight:bold"><h3>Wear Masker</h3></a>
          <p>
             How to use the right mask: <br>
             <ul style="margin-left:30px">
                <li>1. Pick up your mask by its ear loops.</li>
                <li>2. Be sure that it covers your nose and your mouth.</li>
                <li>3. Adjust the fit of the mask to ensure your chin is covered.</li>
                <li>4. Secure the mask around the bridge of your nose.</li>
                <li>5. Make sure to adjust where needed.</li>
                <li>6. When you want to take off the mask, make sure your hands are cleaned.</li>
             </ul>
          </p>
        </article>
        <article>
          <br><a href="#" style="font-weight:bold"><h3>Wash Hand</h3></a>
          <p>
            Follow Five Steps to Wash Your Hands the Right Way Washing your hands is easy, and it’s one of the most effective ways to prevent the spread of germs.
            <ol style="margin-left:30px">
                <li>1. Wet your hands with clean, running water (warm or cold), turn off the tap, and apply soap.</li>
                <li>2. Lather your hands by rubbing them together with the soap. Lather the backs of your hands, between your fingers, and under your nails.</li>
                <li>3. Scrub your hands for at least 20 seconds. Need a timer? Hum the “Happy Birthday” song from beginning to end twice.</li>
                <li>4. Rinse your hands well under clean, running water.</li>
                <li>5. Dry your hands using a clean towel or air dry them.</li>
            </ol>
          </p>
        </article>
        <article>
          <br><a href="#" style="font-weight:bold"><h3>Stay at Home</h3></a>
          <p>
             Various activities that are usually carried out outside the home will be safer if carried out indoors, including:
                <ol style="margin-left:30px">
                    <li>1. Work at home</li>
                    <li>2. Study at home</li>
                    <li>3. Worship at home</li>
                    <li>4. Exercise at home</li>
                    <li>5. Leave the house only for urgent needs</li>
                    <li>6. Use social media to stay in touch with other people</li>
                </ol>
          </p>
        </article>
      </div>
    </section>
    <!--end-protect-->
    <footer>
         <div class="footer">
            <div class="container">
               <div class="row">
                        <div class="col-lg-2 col-md-6 col-sm-6">
                           <div class="hedingh3 text_align_left">
                              <h3>Resources</h3>
                              <ul class="menu_footer">
                                 <li><a href="index.html">Home</a><li>
                                 <li><a href="javascript:void(0)">What we do</a><li>
                                 <li> <a href="javascript:void(0)">Media</a><li>
                                 <li> <a href="javascript:void(0)">Travel Advice</a><li>
                                 <li><a href="javascript:void(0)">Protection</a><li>
                                 <li><a href="javascript:void(0)">Care</a><li>
                              </ul>
                             
           
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                           <div class="hedingh3 text_align_left">
                             <h3>About</h3>
                              <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various</p>
                           </div>
                        </div>                    
                       
                        <div class="col-lg-3 col-md-6 col-sm-6">
                           <div class="hedingh3  text_align_left">
                              <h3>Contact  Us</h3>
                                <ul class="top_infomation">
                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                           Making this the first true  
                        </li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i>
                           Call : +01 1234567890
                        </li>
                        <li><i class="fa fa-envelope" aria-hidden="true"></i>
                           <a href="Javascript:void(0)">Email : demo@gmail.com</a>
                        </li>
                     </ul>
                            
                           
                     </div>
                  </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                           <div class="hedingh3 text_align_left">
                              <h3>countrys</h3>
                              <div class="map">
                                <img src="{{asset('covido-master/images/map.png')}}" alt="#"/>
                              </div>
                           </div>
                        </div>
                    
               </div>
            </div>
            <div class="copyright">
               <div class="container">
                  <div class="row">
                     <div class="col-md-8 offset-md-2">
                        <p>© 2020 All Rights Reserved. Design by <a href="https://html.design/"> Free html Templates</a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <script src="{{asset('covido-master/js/jquery.min.js')}}"></script>
      <script src="{{asset('covido-master/js/bootstrap.bundle.min.js')}}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
      <script src="{{asset('covido-master/js/owl.carousel.min.js')}}"></script>
      <script src="{{asset('covido-master/js/custom.js')}}"></script>
</body>
</html>