<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Covido</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="{{asset('covido-master/css/bootstrap.min.css')}}">
      
      <!-- style css -->
      <link rel="stylesheet" href="{{asset('covido-master/css/style.css')}}">
      <!-- Responsive-->
      <link rel="stylesheet" href="{{asset('covido-master/css/responsive.css')}}">
      <!-- fevicon -->     
       {{-- <link rel="icon" href="{{asset('covido-master/images/fevicon.png')}}" type="image/gif" /> --}}
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
       <link rel="stylesheet" href="{{asset('covido-master/css/owl.carousel.min.css')}}"> 
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
   </head>
   <!-- body -->
   <body class="main-layout inner_page">
      <!-- loader  -->
      <div class="loader_bg">
         <div class="loader"><img src="{{asset('covido-master/images/loading.gif')}}" alt="#"/></div>
      </div>
      <!-- end loader -->
      <!-- top -->
      <!-- header -->
         <header class="header-area">
            <div class="left">
               <a href="Javascript:void(0)"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
            <div class="right">
               <a href="Javascript:void(0)"><i class="fa fa-user" aria-hidden="true"></i></a>
            </div>
            <div class="container">
               <div class="row d_flex">
                  <div class="col-sm-3 logo_sm">
                     <div class="logo">
                        <a href="/home"></a>
                     </div>
                  </div>
                  <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-9">
                     <div class="navbar-area">
                        <nav class="site-navbar">
                           <ul>
                              <li><a href="/">Home</a></li>
                              <li><a href="/about">About</a></li>
                              <li><a href="/action">Card</a></li>
                              <li><a href="/home">Covido</a></li>
                              <li><a class="active" href="/news">News</a></li>
                              <li><a href="/workers">Workers</a></li>
                              <li><a href="/contact">Contact </a></li>
                           </ul>
                           <button class="nav-toggler">
                             <span></span>
                           </button>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </header>
      <!-- end header -->
      <!-- end banner -->
     <!-- coronata -->
     <nav class="nav nav-pills flex-column flex-sm-row">
      <a class="flex-sm-fill text-sm-center nav-link" href="/coronata">Overview</a>
      <a class="flex-sm-fill text-sm-center nav-link active" href="#">Prevention</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/Symptoms">Symptoms</a>
     </nav>
     <div class="container">
     <div class="media">
                    <img class="mr-4"  src="{{asset('covido-master/images/coronata1.jpg')}}" style = "width:350px; height:250px; margin-top:130px;"  alt="..." ><br>                      
                     <div class="media-body" style="margin:70px 50px 70px 50px">
                        <h3 class="mt-5" style="font-weight:bold">Prevention</h3>
                        <p style="text-align:justify">To prevent infection and to slow transmission of COVID-19, do the following:</p>
                            <ol>
                                <li>1. Wash your hands regularly with soap and water, or clean them with alcohol-based hand rub.</li>
                                <li>2. Maintain at least 1 metre distance between you and people coughing or sneezing.</li>
                                <li>3. Avoid touching your face.</li>
                                <li>4. Cover your mouth and nose when coughing or sneezing.</li>
                                <li>5. Stay home if you feel unwell.</li>
                                <li>6. Refrain from smoking and other activities that weaken the lungs.</li>
                                <li>7. Practice physical distancing by avoiding unnecessary travel and staying away from large groups of people.</li>
                           </ol>
                        </div>
                     </div>
                </div>

      <!-- end coronata -->
      <!--  footer -->
      <footer>
         <div class="footer">
            <div class="container">
               <div class="row">
                        <div class="col-lg-2 col-md-6 col-sm-6">
                           <div class="hedingh3 text_align_left">
                              <h3>Resources</h3>
                              <ul class="menu_footer">
                                 <li><a href="index.html">Home</a><li>
                                 <li><a href="javascript:void(0)">What we do</a><li>
                                 <li> <a href="javascript:void(0)">Media</a><li>
                                 <li> <a href="javascript:void(0)">Travel Advice</a><li>
                                 <li><a href="javascript:void(0)">Protection</a><li>
                                 <li><a href="javascript:void(0)">Care</a><li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                           <div class="hedingh3 text_align_left">
                             <h3>About</h3>
                              <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various</p>
                           </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                           <div class="hedingh3  text_align_left">
                              <h3>Contact  Us</h3>
                              <ul class="top_infomation">
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                Making this the first true</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i>
                                Call : +01 1234567890 </li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i>
                                <a href="Javascript:void(0)">Email : demo@gmail.com</a></li>
                              </ul>  
                           </div>
                        </div>
                     <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="hedingh3 text_align_left">
                              <h3>countrys</h3>
                           <div class="map">
                              <img src="{{asset('covido-master/images/map.png')}}" alt="#"/>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            <div class="copyright">
               <div class="container">
                  <div class="row">
                     <div class="col-md-8 offset-md-2">
                        <p>© 2020 All Rights Reserved. Design by <a href="https://html.design/"> Free html Templates</a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- end footer -->
      <!-- Javascript files-->
      <script src="{{asset('covido-master/js/jquery.min.js')}}"></script>
      <script src="{{asset('covido-master/js/bootstrap.bundle.min.js"')}}></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
      <script src="{{asset('covido-master/js/owl.carousel.min.js')}}"></script>
      <script src="{{asset('covido-master/js/custom.js')}}"></script>
   </body>
</html>