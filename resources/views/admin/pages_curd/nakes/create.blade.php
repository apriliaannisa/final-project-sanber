@extends('admin.master')

@section('header')
    ADMIN CURD
@endsection

@section('TableTitle')
  Tambah Nakes 
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="string" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="posisi">Posisi</label>
        <input type="string" class="form-control" name="posisi" id="posisi" placeholder="Masukkan posisi">
        @error('posisi')
            <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea  type="text" class="form-control" name="bio" id="bio" rows="3"></textarea>
        @error('bio')
            <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror
    </div>
    <div class="custom-file mb-3">
        <input type="file" class="custom-file-input" name ="foto" id="foto">
        <label class="custom-file-label" for="customFile">Upload Picture</label>
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection