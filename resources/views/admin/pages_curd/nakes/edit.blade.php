@extends('admin.master')

@section('header')
    ADMIN CURD
@endsection

@section('TableTitle')
    Edit Nakes Id-{{ $nakes->id }}
@endsection

@section('content')
    
<div>
    <form action="/nakes/{{$nakes->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="string" class="form-control" name="nama" value="{{$nakes->nama}}" id="nama" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="posisi">posisi</label>
            <input type="string" class="form-control" name="posisi"  value="{{$nakes->posisi}}"  id="posisi" placeholder="Masukkan posisi">
            @error('posisi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">bio</label>
            <textarea  type="text" class="form-control" name="bio" id="bio" value="{{$nakes->bio}}" rows="3"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="custom-file mb-3">
            <input type="file" class="custom-file-input" name ="foto" id="foto" value="{{$nakes->foto}}">
            <label class="custom-file-label" for="customFile">Upload Picture</label>
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection