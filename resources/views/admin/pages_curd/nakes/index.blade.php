@extends('admin.master')

@section('header')
    ADMIN CURD
@endsection

@section('TableTitle')
  List nakes 
@endsection

@section('content')
<!-- Main content -->
  
      <a href="/nakes/create" class="btn btn-primary mb-2">Tambah</a>
      <table class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">nama</th>
              <th scope="col">posisi</th>
              <th scope="col">bio</th>
            </tr>
          </thead>
          <tbody>
              @forelse ($nakes as $key=>$value)
                  <tr>
                      <td>{{$key + 1}}</th>
                      <td>{{$value->nama}}</td>
                      <td>{{$value->posisi}}</td>
                      <td>{{$value->bio}}</td>
                      <td>
                          <a href="/nakes/{{$value->id}}" class="btn btn-info">Detail</a>
                          <a href="/nakes/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                          <form action="/nakes/{{$value->id}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <input type="submit" class="btn btn-danger my-1" value="Delete">
                          </form>
                      </td>
                  </tr>
              @empty
                  <tr colspan="3">
                      <td>No data</td>
                  </tr>  
              @endforelse              
          </tbody>
      </table>
    
    @endsection