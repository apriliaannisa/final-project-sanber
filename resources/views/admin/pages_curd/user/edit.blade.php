@extends('admin.master')

@section('header')
    ADMIN CURD
@endsection

@section('TableTitle')
    Edit Berita Id-{{ $berita->id }}
@endsection

@section('content')
    
<div>
    <h2>Edit berita {{$berita->id}}</h2>
    <form action="/berita/{{$berita->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">judul</label>
            <input type="string" class="form-control" name="judul" value="{{$berita->judul}}" id="judul" placeholder="Masukkan judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="konten">konten</label>
            <textarea  type="text" class="form-control" name="konten" id="konten" value="{{$berita->konten}}" rows="3"></textarea>
            @error('konten')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="penulis">penulis</label>
            <input type="string" class="form-control" name="penulis"  value="{{$berita->penulis}}"  id="penulis" placeholder="Masukkan penulis">
            @error('penulis')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="custom-file mb-3">
            <input type="file" class="custom-file-input" name ="foto" id="foto" value="{{$berita->foto}}">
            <label class="custom-file-label" for="customFile">Upload Picture</label>
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection