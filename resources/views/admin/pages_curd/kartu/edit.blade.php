@extends('admin.master')

@section('header')
    ADMIN CURD
@endsection

@section('TableTitle')
    Edit kartu_ucapan Id-{{ $kartu_ucapan->id }}
@endsection

@section('content')
    
<div>
    <h2>Edit kartu_ucapan {{$kartu_ucapan->id}}</h2>
    <form action="/kartu_ucapan/{{$kartu_ucapan->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">Judul</label>
            <input type="string" class="form-control" name="judul" value="{{$kartu_ucapan->judul}}" id="judul" placeholder="Masukkan judul">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="user_id">Penulis</label>
            <input type="string" class="form-control" name="user_id"  value="{{$kartu_ucapan->user_id}}"  id="user_id" placeholder="Masukkan id user">
            @error('user_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="nakes_id">Penerima</label>
            <input type="string" class="form-control" name="nakes_id"  value="{{$kartu_ucapan->nakes_id}}"  id="nakes_id" placeholder="Masukkan id nakes">
            @error('nakes_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="konten">Konten</label>
            <textarea  type="text" class="form-control" name="konten" id="konten" value="{{$kartu_ucapan->konten}}" rows="3"></textarea>
            @error('konten')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="custom-file mb-3">
            <input type="file" class="custom-file-input" name ="foto" id="foto" value="{{$kartu_ucapan->foto}}">
            <label class="custom-file-label" for="customFile">Upload Picture</label>
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection