@extends('admin.master')

@section('header')
    ADMIN CURD
@endsection

@section('TableTitle')
  List kartu ucapan 
@endsection

@section('content')
<!-- Main content -->
  
      <a href="/kartu-ucapan/create" class="btn btn-primary mb-2">Tambah</a>
      <table class="table">
          <thead class="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Judul</th>
              <th scope="col">Penulis</th>
              <th scope="col">Penerima</th>
              <th scope="col">Konten</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
              @forelse ($kartu_ucapan as $key=>$value)
                  <tr>
                      <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->user_id}}</td>
                        <td>{{$value->nakes_id}}</td>
                        <td>{{$value->konten}}</td>
                      <td>
                          <a href="/kartu-ucapan/{{$value->id}}" class="btn btn-info">Detail</a>
                          <a href="/kartu-ucapan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                          <form action="/kartu-ucapan/{{$value->id}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <input type="submit" class="btn btn-danger my-1" value="Delete">
                          </form>
                      </td>
                  </tr>
              @empty
                  <tr colspan="3">
                      <td>No data</td>
                  </tr>  
              @endforelse              
          </tbody>
      </table>
    
    @endsection