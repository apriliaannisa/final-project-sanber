@extends('admin.master')

@section('header')
    ADMIN CURD
@endsection

@section('TableTitle')
  Tambah Berita 
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="judul">Judul</label>
        <input type="string" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
        @error('judul')
            <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="penulis">Penulis</label>
        <input type="string" class="form-control" name="penulis" id="penulis" placeholder="Masukkan penulis">
        @error('penulis')
            <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="konten">Konten</label>
        <textarea  type="text" class="form-control" name="konten" id="konten" rows="3"></textarea>
        @error('konten')
            <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @enderror
    </div>
    <div class="custom-file mb-3">
        <input type="file" class="custom-file-input" name ="foto" id="foto">
        <label class="custom-file-label" for="customFile">Upload Picture</label>
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection