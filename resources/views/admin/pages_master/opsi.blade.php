@extends('admin.master')

@section('header')
    Admin Dashboard
@endsection

@section('TableTitle')
  Opsi
@endsection

@section('content')
<!-- Main content -->
  
    <a type="button" class="btn btn-primary btn-lg" href="{{ url('/admin/get-user') }}">Cetak Data Pengguna (PDF)</a>
    <a type="button" class="btn btn-primary btn-lg" href="{{ url('/admin/excel-user') }}">Cetak Data Nakes (Excel)</a>
@endsection