<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKartuUcapanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kartu_ucapan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('nakes_id');
            $table->string('judul', 50);
            $table->text('konten');
            $table->binary('foto');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('nakes_id')->references('id')->on('nakes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kartu_ucapan');
    }
}
