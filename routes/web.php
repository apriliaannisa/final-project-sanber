<?php

Route::get('/', 'HomeController@index');

Route::get('/about', function () {
    return view('halaman.about');
});

Route::get('/contact', function () {
    return view('halaman.contact');
});

Route::get('/workers', function () {
    return view('halaman.workers');
});

Route::get('/action', function () {
    return view('halaman.action');
});

Route::get('/news', function () {
    return view('halaman.news');
});

Route::get('/home', function () {
    return view('layout.master');
});

Route::get('/coronata', function () {
    return view('halaman.coronata');
});

Route::get('/prevention', function () {
    return view('halaman.coronata.prevention');
});

Route::get('/Symptoms', function () {
    return view('halaman.coronata.Symptoms');
});

Route::get('/protect', function () {
    return view('halaman.protect');
});

Route::get('/cases', function () {
    return view('halaman.cases');
});

Route::get('/card', function () {
    return view('halaman.card');
});

// Route::get('/admin', 'AdminController@index');

Route::get('/berita', 'Corotana\BeritaController@index');

Route::get('/berita/create', 'Corotana\BeritaController@create');

Route::post('/berita', 'Corotana\BeritaController@store');

Route::get('/berita/{berita_id}', 'Corotana\BeritaController@show');

Route::get('/berita/{berita_id}/edit', 'Corotana\BeritaController@edit');

Route::put('/berita/{berita_id}', 'Corotana\BeritaController@update');

Route::delete('/berita/{berita_id}', 'Corotana\BeritaController@destroy');

// 

Route::get('/nakes', 'Corotana\NakesController@index');

Route::get('/nakes/create', 'Corotana\NakesController@create');

Route::post('/nakes', 'Corotana\NakesController@store');

Route::get('/nakes/{nakes_id}', 'Corotana\NakesController@show');

Route::get('/nakes/{nakes_id}/edit', 'Corotana\NakesController@edit');

Route::put('/nakes/{nakes_id}', 'Corotana\NakesController@update');

Route::delete('/nakes/{nakes_id}', 'Corotana\NakesController@destroy');

// 

Route::get('/kartu-ucapan', 'Corotana\KartuUcapanController@index');

Route::get('/kartu-ucapan/create', 'Corotana\KartuUcapanController@create');

Route::post('/kartu-ucapan', 'Corotana\KartuUcapanController@store');

Route::get('/kartu-ucapan/{kartu-ucapan_id}', 'Corotana\KartuUcapanController@show');

Route::get('/kartu-ucapan/{kartu-ucapan_id}/edit', 'Corotana\KartuUcapanController@edit');

Route::put('/kartu-ucapan/{kartu-ucapan_id}', 'Corotana\KartuUcapanController@update');

// 
Auth::routes();

// DOMPDF

Route::get('/admin', 'Corotana\AdminController@home');

Route::get('/admin/get-user', 'Corotana\AdminController@getUser');

Route::get('/admin/excel-user', 'Corotana\AdminController@export');
